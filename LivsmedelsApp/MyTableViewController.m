//
//  MyTableViewController.m
//  LivsmedelsApp
//
//  Created by ITHS on 2016-03-03.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "MyTableViewController.h"
#import "CustomCell.h"
#import "NSXFood.h"
#import "DetailsViewController.h"
#import "FavoritesTableViewController.h"

@interface MyTableViewController ()

@end

@implementation MyTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.favorites = [[NSMutableArray alloc] init];
    
    NSUserDefaults *settings = [NSUserDefaults standardUserDefaults];
    NSData *data = [settings objectForKey:@"favoritesList"];
    NSMutableArray *favoritesList = [[NSKeyedUnarchiver unarchiveObjectWithData:data] mutableCopy];
    self.favorites = [[NSMutableArray alloc] initWithArray:favoritesList];
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];

    
    self.searchController.searchResultsUpdater = self;
    self.definesPresentationContext = YES;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.tableView.tableHeaderView = self.searchController.searchBar;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 64;
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    
    NSString *searchText = searchController.searchBar.text;
    
    if([searchText length] > 1) {
    
    NSString *s = [NSString stringWithFormat:@"http://matapi.se/foodstuff?query=%@&format=json&pretty=1", searchText];
    
    NSString *escaped = [s stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *url = [NSURL URLWithString:escaped];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session =[NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error) {
            NSLog(@"Error: %@", error);
            return;
        }
        
        NSError *jsonParseError = nil;
        
        self.nameResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonParseError];
        
        if(jsonParseError) {
            NSLog(@"Failed to parse data: %@", jsonParseError);
            return;
        }
        
        int n = [self.nameResult count];
        if(n > 20) {
            n = 20;
        }
        
        NSMutableArray *foodNumbers = [[NSMutableArray alloc] init];
        
        for(int i=0; i < n; i++){
            NSDictionary *dict = self.nameResult[i];
            //NSLog(@"%@", self.foodResult[@"number"]);
            [foodNumbers addObject:dict[@"number"]];
            //NSLog(@"%@",dict[@"number"]);
            
        }
        
        NSMutableArray* foods = [[NSMutableArray alloc] init];
        
        for(int i = 0; i < [foodNumbers count]; i++){
            
            NSString *s = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%@", foodNumbers[i]];
            
            NSString *escaped = [s stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            NSURL *url = [NSURL URLWithString:escaped];
            
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            NSURLSession *session =[NSURLSession sharedSession];
            
            NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                if(error) {
                    NSLog(@"Error: %@", error);
                    return;
                }
                
                NSError *jsonParseError = nil;
                
                self.foodResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonParseError];
                
                if(jsonParseError) {
                    NSLog(@"Failed to parse data: %@", jsonParseError);
                    return;
                }
                
                NSString* str = [[NSString alloc] init];
                str = [NSString stringWithFormat:@"%d", i];
                NSXFood *food = [[NSXFood alloc] init];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    food.name = self.foodResult[@"name"];
                    NSDictionary *nutrients = self.foodResult[@"nutrientValues"];
                    food.energy = ((NSNumber*)nutrients[@"energyKcal"]).stringValue;
                    food.fat =  ((NSNumber*)nutrients[@"fat"]).stringValue;
                    food.protein = ((NSNumber*)nutrients[@"protein"]).stringValue;
                    [self.tableView reloadData];
                });
                //NSLog(@"%@", self.foodResult);
                [foods addObject:food];
                
            }];
            [task resume];
            
            
        }
        
        self.data = foods;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
        
    }];
    [task resume];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
        return self.data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CustomCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.food = self.data[indexPath.row];
    
    cell.nameLabel.text = cell.food.name;
    cell.energyLabel.text = cell.food.energy;
    cell.proteinLabel.text = cell.food.protein;
    cell.fatLabel.text = cell.food.fat;
    
    return cell;
    
}

- (IBAction)addToFavorites:(UIButton *)sender {
    CustomCell *clickedCell = (CustomCell *)[[sender superview] superview];
    [self.favorites addObject:clickedCell.food];
    
    NSUserDefaults *settings = [NSUserDefaults standardUserDefaults];
    [settings setObject:[NSKeyedArchiver archivedDataWithRootObject:self.favorites] forKey:@"favoritesList"];
    [settings synchronize];
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetails"]){
        
        CustomCell *cell = sender;
        
        DetailsViewController *controller = (DetailsViewController *) segue.destinationViewController;
        controller.food = cell.food;
    }
    else if([[segue identifier] isEqualToString:@"showFavorites"]){
        FavoritesTableViewController *controller2 = (FavoritesTableViewController *) segue.destinationViewController;
        controller2.favorites = self.favorites;
    }
}

@end
