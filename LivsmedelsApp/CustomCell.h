//
//  CustomCell.h
//  LivsmedelsApp
//
//  Created by ITHS on 2016-03-01.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSXFood.h"

@interface CustomCell : UITableViewCell 

@property (weak, nonatomic) IBOutlet UIImageView *foodImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *energyLabel;
@property (weak, nonatomic) IBOutlet UILabel *proteinLabel;
@property (weak, nonatomic) IBOutlet UILabel *fatLabel;
@property (weak, nonatomic) IBOutlet UISwitch *favoriteSwitch;
@property NSXFood *food;


@end
