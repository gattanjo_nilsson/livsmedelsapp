//
//  DetailsViewController.m
//  LivsmedelsApp
//
//  Created by ITHS on 2016-03-12.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "DetailsViewController.h"

@interface DetailsViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *energyLabel;
@property (weak, nonatomic) IBOutlet UILabel *proteinLabel;
@property (weak, nonatomic) IBOutlet UILabel *fatLabel;
@property (weak, nonatomic) IBOutlet UILabel *healthValueLabel;
@property (weak, nonatomic) IBOutlet UIImageView *santa;
@property (nonatomic) UIDynamicAnimator *animator;
@property (nonatomic) UIGravityBehavior *gravity;
@property (nonatomic) UICollisionBehavior *collision;
@property (nonatomic) UIImageView *square;

@end

@implementation DetailsViewController

- (IBAction)onTap:(id)sender {
    self.square.center = [sender locationInView:self.view ];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.nameLabel.text = self.food.name;
    self.imageView.image = self.food.foodImage;
    self.energyLabel.text = [NSString stringWithFormat:@"%@ kcal",self.food.energy];
    self.proteinLabel.text = [NSString stringWithFormat:@"%@ g",self.food.protein];
    self.fatLabel.text = [NSString stringWithFormat:@"%@ g",self.food.fat];
    
    float fat = [self.food.fat floatValue];
    float protein = [self.food.protein floatValue];
    float energy = [self.food.energy floatValue];
    
    int healthValue = fat * protein * energy;
    self.healthValueLabel.text = [NSString stringWithFormat:@"%d p", healthValue];
    
    self.square = [[UIImageView alloc] initWithFrame:CGRectMake(140, 40, 32, 32)];
    self.square.image=[UIImage imageNamed:@"snowflake.png"];

    [self.view addSubview:self.square];
    
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    self.gravity = [[UIGravityBehavior alloc] initWithItems:@[self.square]];
    self.collision = [[UICollisionBehavior alloc] initWithItems:@[self.square]];
    self.collision.translatesReferenceBoundsIntoBoundary = YES;
    
    [self.animator addBehavior:self.gravity];
    [self.animator addBehavior:self.collision];
    

        [UIView animateWithDuration:2.0
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.santa.center = CGPointMake(-800, self.santa.frame.origin.y);
                         }
                         completion:^(BOOL finished){
                             self.santa.hidden = YES;
                         }];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
