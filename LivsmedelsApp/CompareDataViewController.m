//
//  CompareDataViewController.m
//  LivsmedelsApp
//
//  Created by ITHS on 2016-04-03.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "CompareDataViewController.h"
#import "NSXFood.h"

@interface CompareDataViewController ()


@property (weak, nonatomic) IBOutlet UIPickerView *picker;
@property (weak, nonatomic) IBOutlet GKBarGraph *Graph;

@end

@implementation CompareDataViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.picker.delegate = self;
    self.Graph.dataSource = self;
    [self.Graph draw];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self.Graph draw];
}

-(NSInteger)numberOfBars {
    return 6;
}

-(NSNumber *)valueForBarAtIndex:(NSInteger)index {
    switch (index) {
        case 0:
            return @([self.food1.energy floatValue]);
            break;
        case 1:
            return @([self.food1.protein floatValue]);
            break;
        case 2:
            return @([self.food1.fat floatValue]);
            break;
        case 3:
            return @([self.food2.energy floatValue]);
            break;
        case 4:
            return @([self.food2.protein floatValue]);
            break;
        case 5:
            return @([self.food2.fat floatValue]);
            break;
        default:
            break;
    }
    return @(0);
}

-(NSString *)titleForBarAtIndex:(NSInteger)index {
    switch (index) {
        case 0:
            return @"Energy";
            break;
        case 1:
            return @"Protein";
            break;
        case 2:
            return @"Fat";
            break;
        case 3:
            return @"Energy";
            break;
        case 4:
            return @"Protein";
            break;
        case 5:
            return @"Fat";
            break;
        default:
            break;
    }
    return 0;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    switch (component) {
        case 0:
            return [self.favorites count];
            break;
        case 1:
            return [self.favorites count];
            break;
        default:
            break;
    }
    return 1;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSXFood *food = [self.favorites objectAtIndex:row];
    switch (component) {
        case 0:
            return food.name;
            break;
        case 1:
            return food.name;
            break;
        default:
            break;
    }
    return @"LOL";
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    switch (component) {
        case 0:
            self.food1 = [self.favorites objectAtIndex:row];
            break;
        case 1:
            self.food2 = [self.favorites objectAtIndex:row];
            break;
        default:
            break;
    }
    [self.Graph draw];
}

- (UIColor *)colorForBarAtIndex:(NSInteger)index {
    switch (index) {
        case 0:
            return [UIColor blueColor];
            break;
        case 1:
            return [UIColor blueColor];
            break;
        case 2:
            return [UIColor blueColor];
            break;
        case 3:
            return [UIColor redColor];
            break;
        case 4:
            return [UIColor redColor];
            break;
        case 5:
            return [UIColor redColor];
            break;
        default:
            break;
    }
    return 0;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
