//
//  NSXFood.h
//  LivsmedelsApp
//
//  Created by ITHS on 2016-03-06.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>


@interface NSXFood : NSObject

@property NSString *name;
@property NSString *energy;
@property NSString *protein;
@property NSString *fat;
@property UIImage *foodImage;

@end
