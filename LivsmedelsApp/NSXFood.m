//
//  NSXFood.m
//  LivsmedelsApp
//
//  Created by ITHS on 2016-03-06.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import "NSXFood.h"

@implementation NSXFood

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:self.name forKey:@"name"];
    [coder encodeObject:self.energy forKey:@"energy"];
    [coder encodeObject:self.protein forKey:@"protein"];
    [coder encodeObject:self.fat forKey:@"fat"];
    [coder encodeObject:self.foodImage forKey:@"foodImage"];
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self) {
        _name = [coder decodeObjectForKey:@"name"];
        _energy = [coder decodeObjectForKey:@"energy"];
        _protein = [coder decodeObjectForKey:@"protein"];
        _fat = [coder decodeObjectForKey:@"fat"];
        _foodImage = [coder decodeObjectForKey:@"foodImage"];
    }
    return self;
}

@end
