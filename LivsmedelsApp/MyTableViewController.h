//
//  MyTableViewController.h
//  LivsmedelsApp
//
//  Created by ITHS on 2016-03-06.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTableViewController : UITableViewController

@property NSMutableArray *data;
@property NSArray *searchResult;
@property UISearchController *searchController;
@property NSArray *nameResult;
@property NSDictionary *foodResult;
@property NSMutableArray *favorites;

@end
