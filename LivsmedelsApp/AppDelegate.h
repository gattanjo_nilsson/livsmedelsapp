//
//  AppDelegate.h
//  LivsmedelsApp
//
//  Created by ITHS on 2016-03-01.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

