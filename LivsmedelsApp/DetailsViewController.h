//
//  DetailsViewController.h
//  LivsmedelsApp
//
//  Created by ITHS on 2016-03-12.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSXFood.h"

@interface DetailsViewController : UIViewController

@property NSXFood *food;
@end
