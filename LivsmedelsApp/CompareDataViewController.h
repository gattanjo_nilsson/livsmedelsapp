//
//  CompareDataViewController.h
//  LivsmedelsApp
//
//  Created by ITHS on 2016-04-03.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GKBarGraph.h>
#import "NSXFood.h"

@interface CompareDataViewController : UIViewController <GKBarGraphDataSource, UIPickerViewDataSource, UIPickerViewDelegate>

@property NSMutableArray *favorites;
@property NSXFood *food1;
@property NSXFood *food2;

@end
