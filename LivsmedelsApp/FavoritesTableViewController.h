//
//  FavoritesTableViewController.h
//  LivsmedelsApp
//
//  Created by ITHS on 2016-03-12.
//  Copyright © 2016 ITHS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavoritesTableViewController : UITableViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property NSMutableArray *favorites;
@property NSIndexPath *index;

- (IBAction)takePhoto:(id)sender;

@end
